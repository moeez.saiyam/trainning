<?php
/**
*Functions implementation in PHP
*/

/**
*Simple Addition Function
*/

function add($num1, $num2)
{
    echo "I'm going to add\n";
    $sum = $num1 + $num2;
    print "Sum is: $sum";
}

function add1($num1, $num2)
{
    echo "I'm going to add\n";
    $sum = $num1 + $num2;
    return $sum;    
}

/**
*Function within function
*/
function sum()
{
    echo "In SUM";
    function add2($a, $b)
    {
        $sum = $a + $b;
        print "Sum is: $sum";
    }
}

/**
*Recursive Function
*/
function recursion($number)
{
    if ($number < 10)
    {
        echo $number;
        recursion(++$number);
    }
}

/**
*Call by reference function
*Self multiplication of number
*/

function selfmul(&$number)
{
    $number *= $number;
    return $number;
}

//simple add
$a = 5;
$b = 7;
add($a,$b);

//returning add
$c = add1($a,$b);
print $c;

//function within function
sum();
add2(2, 3);

//recursive function
recursion(6);

//call by reference
$num = 3;
selfmul($num);
echo $num;


?>
