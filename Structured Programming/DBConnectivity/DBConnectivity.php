<?php
/**
* Db conectivity
*/
function connection()
{
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "123";
    $dbname = "Student";
    $conn = new mysqli($dbhost, $dbuser, $dbpass,$dbname);
    
    if(!$conn){
       die('Could not connect: ' . mysqli_connect_error());
    }
    return $conn; 
}

function closeConnection($conn)
{
    $conn->close();
    echo "Connection Closed\n";
}

/**
* Insert in db
*/
function insertDB ($id,$name,$inst)
{
    $conn = connection();
    $sql = "insert into info (id, name, institution)
    values ('$id', '$name', '$inst')";
    if ($conn->query($sql) === TRUE){
        echo "Successful";
    }
    else {
        echo "Error";
    }
}

/**
* Get Details
*/
function showInfo()
{
    $conn = connection();
    $sql = "select id, name, institution from info";
    $result = $conn->query($sql);
    if ($result -> num_rows > 0)
    {
        while($row = $result->fetch_assoc()) {
        echo "id: " . $row["id"]. " - Name: " . $row["name"]. " " . $row["institution"]. "\n";
        }
    } else {
        echo "0 results";
      }
}

//$id = 5;
//$name = "Saffi";
//$inst = "RT";
//insertDB($id, $name, $inst);
showInfo();

?>
