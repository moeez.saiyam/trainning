<?php
/**
* My Proj namespace
*/
namespace MyProj

{
    function Sum($a,$b)
    {
        return $a+$b;
    }
}

/**
* My Proj1 namespace
*/
namespace MyProj1
{ 
    function Sum($a,$b,$c)
    {
        return $a+$b+$c;
    }
}

namespace
{
    $a = MyProj\Sum(1,3);
    echo $a ."\n";
    $b = MyProj1\Sum(1,3,6);
    echo $b;
}
?>
