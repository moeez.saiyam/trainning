<?php

set_time_limit(0);
 
$ip = '127.0.0.1';
$port = 1935;
 
/**
* socket create
*/
$sock = socket_create(AF_INET,SOCK_STREAM,SOL_TCP)
if($sock < 0) {
    echo "socket_create() Fail to create:".socket_strerror($sock)."\n";
}
 
/**
* socket bind
*/
$ret = socket_bind($sock,$ip,$port)
if($ret < 0) {
    echo "socket_bind() Fail to bind:".socket_strerror($ret)."\n";
}
 
/**
* socket listen
*/
$ret = socket_listen($sock,4)
if($ret < 0) {
    echo "socket_listen() Fail to listen:".socket_strerror($ret)."\n";
}
 
$count = 0;
 
do {
	/**
	* socket accept
	*/
    if (($msgsock = socket_accept($sock)) < 0) {
        echo "socket_accept() failed: reason: " . socket_strerror($msgsock) . "\n";
        break;
    } else {
 
	/**
	* socket write
	*/
        $msg ="Success receive from client！\n";
        socket_write($msgsock, $msg, strlen($msg));
	
	/**
	* socket read
	*/
        echo "Success\n";
        $buf = socket_read($msgsock,8192);
 
 
        $talkback = "Received Message:$buf\n";
        echo $talkback;
 
        if(++$count >= 5){
            break;
        };
 
 
    }
    //echo $buf;
    socket_close($msgsock);
 
} while (true);
 
/**
* socket read
*/
socket_close($sock);


?>
