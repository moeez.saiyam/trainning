<?php
/**
*Global Scope of variable a
*/
$a = 1;

function check()
{
    global $a;
    echo "Inside Func: $a\n";
    $a++;
    echo "After increment: $a\n";
}
$a++;
echo "Outside func: $a\n";
check(); //function call
$a++;
echo "After func call: $a\n";
?>
