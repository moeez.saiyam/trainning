<?php
/**
*Include a function Vars.php and get color and fruit
*Also implemented scope of variable
*/
$fruit = "orange";
function test()
{
    global $color;
    include 'vars.php';
    echo "A $color $fruit\n";
}

test();
echo "A $color $fruit\n";

?>
