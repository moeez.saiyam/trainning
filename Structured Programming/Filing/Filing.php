<?php
/**
*READING CONTENT FROM FILE
*/

$data = "file.txt";

// Check the existence of file
if(file_exists($data)){
    // Open the file for reading
    $handle = fopen($data, "r") or die("ERROR: Cannot open the file.");
    // Read from the file
    $content = fread($handle, filesize($data));
    //closing file
    fclose($handle);
    // Display the file content    
    echo $content;
}
else{
    echo "ERROR";
}

//we can also use readfile() for reading all contents from file
?>
