<?php

class Car {
    private $name;
    private $model;
    private $color;
    private $isSports = "NO";
    function __construct($name = null, $model = null, $color = null)
    {
        if ($name && $model && $color)
        {
             $this->name = $name;
             $this->model = $model;
             
        }
    }
    public function display()
    {
         echo "CAR: $this->name"."\n";
         echo "COLOR: $this->color"."\n";
         echo "MODEL: $this->model"."\n";
         
    }
    public function getName(){
         return $this->name;
    }
}

class SportsCar extends Car {
  private $style = 'fast and furious';
 
  public function driveItWithStyle()
  {
    return 'Drive a '  . $this -> getName() . $this -> style ;
  }

}

$mycar = new SportsCar("Ferari", "XYZ", "black");
$mycar->display();
echo $mycar->driveItWithStyle();



?>
