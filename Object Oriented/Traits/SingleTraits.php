<?php
trait Hello {
    public function sayHello() {
        echo 'Hello ';
    }
}

class MyHelloWorld {
    use Hello;
    public function sayExclamationMark() {
        echo '!';
    }
}

$o = new MyHelloWorld();
$o->sayHello();
$o->sayExclamationMark();
?>
