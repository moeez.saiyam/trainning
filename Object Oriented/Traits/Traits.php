<?php

class Base {

    public function printSomething() {
        echo "In the base class \n";
    }
}

trait saySomething {

    public function printSomething() {
        parent::printSomething();
        echo "Trait Activated\n";
    }

}

class Child extends Base {
    
    use saySomething;

}

$o = new Child ();
$o->printSomething();

?>
