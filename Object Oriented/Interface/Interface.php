<?php

interface Vehicle {
    public function setModel ($model);
    public function getModel ();
}

interface Specs {
    public function setSpecs ($specs);
    public function getSpecs ();
}

class Toyota implements Vehicle {
    
    private $model;
    public function setModel ($model)
    {
        $this->model = $model; 
    }
    public function getModel ()
    {
        return $this->model;
    }
}

class Honda implements Vehicle, Specs {
    
    private $model;
    private $specs;
    public function setModel ($model)
    {
        $this->model = $model; 
    }
    public function getModel ()
    {
        return $this->model;
    }
    public function setSpecs ($specs)
    {
        $this->specs = $specs; 
    }
    public function getSpecs ()
    {
        return $this->specs;
    }

}


$toyota = new Toyota ();
$toyota->setModel (2008);
echo "MODEL: " . $toyota->getModel() . "\n";

$bike = new Honda ();
$bike->setModel("CD70");
$bike->setSpecs("A two wheel vehicle having color black");
echo "MODEL: " . $bike->getModel() . " ";
echo $bike->getSpecs();

?>
