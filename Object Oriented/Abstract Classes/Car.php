<?php

/**
* An Abstract class Car with abstract function details
*/
abstract class Car {

    protected $milage;
    public function setMilage ($milage)
    {
        $this->milage = $milage;
    }
    
    abstract public function details();

}

/**
* A toyota class
*/
class Toyota extends Car {

     public function details()
     {
         echo "A Toyota Car with milage: " . $this->milage . "\n";
     }
     public function color() {
          echo "BLUE CAR";
     }
}

/**
* A honda class
*/
class Honda extends Car {

     public function details()
     {
         echo "A Honda Car with milage: " . $this->milage . "\n";
     }
     public function color() {
          echo "BLUE CAR";
     }
}

$car = new Toyota();
$car -> setMilage (50);
$car -> details();
$car = new Honda();
$car -> setMilage (60);
$car -> details();


?>
