<?php

interface Shape {

    public function calcArea ();
}

class Circle implements Shape {

    private $radius;

    public function __construct ($radius = null)
    {
        if ($radius)
        {
            $this->radius = $radius;
        }
    }

    public function setRadius ($radius)
    {
        $this->radius = $radius;
    }

    public function getRadius()
    {
        return $this->radius;
    }

    public function calcArea ()
    {
        return $this->radius * $this->radius * pi();
    }

}

class Square implements Shape {

    private $length;
    private $width;

    public function __construct ($length = null, $width = null)
    {
        if ($length && $width)
        {
            $this->length = $length;
            $this->width = $width;
        }
    }

    public function setLength ($length)
    {
        $this->length = $length;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setWidth ($length)
    {
        $this->width = $width;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function calcArea ()
    {
        return $this->length * $this->width;
    }

}

$cir = new Circle();
$cir->setRadius(3);
echo "AREA OF CIRCLE: " . $cir->calcArea() . "\n";
$sq = new Square(2,3);
echo "AREA OF Square: " . $sq->calcArea() . "\n";

?>
