<?php

class Animal {

    private $id;
    protected $name;
    public $details;

    public function setId ($id)
    {
        $this->id = $id;
    }

    public function getId ()
    {
        echo $this->id;
    }

    public function setname ($name)
    {
        $this->name = $name;
    }

    public function getname ()
    {
        echo $this->name;
    }
    public function setdetails ($details)
    {
        $this->details = $details;
    }

    public function getdetails ()
    {
        echo $this->details;
    }

}

class Cat extends Animal {

    public function catname()
    {
        echo $this->name;
    }
}

$o = new Cat;
$o->details = "Accessing Animal from driver";
$o->setId(1);
$o->setname("cat");
echo $o->details;
$o->getid();

?>
