<?php

/**
* A Student class
*/
class Student{

    private $name;
    private $rollno;
    private $institution;


    public function __construct($name = null, $rollno = null, $institution = null) {
        if ($name && $rollno && $institution){
        $this->name = $name;
        $this->rollno = $rollno;
        $this->institution = $institution;
        }
    }


    public function getName(){
        return $this->name;
    }

    public function getRollNo(){
        return $this->rollno;
    }

    public function getInstitution(){
        return $this->institution;
    }

    public function setName($name){
        $this->name=$name;
    }

    public function setRollNO($rollno){
        $this->rollno = $rollno;
    }

    public function setInstitution($institution){
        $this->institution = $institution;
    }

    public function showDetails() {
        echo "NAME: $this->name" . "\n";
        echo "Rollno: $this->rollno" . "\n";
        echo "Institution: $this->institution" . "\n";
    }

    public function addDetails($name, $rollno, $institution) {
        $this->name = $name;
        $this->rollno = $rollno;
        $this->institution = $institution;
    }
}

?>
