<?php
/**
*Type Casting
*/
$str_val = "This is string";
$int_val = (int)$str_val;
var_dump($int_val);

$a = 0;
$check = (bool)$a;
var_dump($check);

$float_var = 3.66;
var_dump((int)$float_var);

/**
*Input a number and convert to intand string
*/
$msg="enter a float:";
echo $msg;
/**takes the float value*/
$a=fgets(STDIN, 1024);
/**trim function avoids the whitespaces of the user input*/
$a=trim($a);
/**converts the float value in to integer*/
$k=(int)$a;
/**displays the value of integer*/
echo "the int value:$k\n";
/**converts the float value in to the string*/
$k=(string)$a;
var_dump($k);
/**displays the value of string*/
echo "converting into string:$k\n";

/**
*breaking string to array
*/
$str = "Hello world. It's a beautiful day.";
print_r (explode(" ",$str));
?>
