<?php
/**
*Declaring Variables
*/

/**
*PHP Integers
*/
$a = 123; /** decimal number */
var_dump($a);
 
$b = -123; /** a negative number */
var_dump($b);
 
$c = 0x1A; /** hexadecimal number */
var_dump($c);
 
$d = 0123; /** octal number */
var_dump($d);

/**
*PHP Strings
*/
$a = 'Hello world!';
echo $a."\n";
 
$b = "Hello world!";
echo $b."\n";
 
$c = 'Stay here, I\'ll be back.';
echo $c."\n";

$d = "Stay here, I'll be back";
echo $d."\n";

/**
*PHP Floating Point Numbers or Doubles
*/
$a = 1.234;
var_dump($a);
 
$b = 10.2e3;
var_dump($b);
 
$c = 4E-2;
var_dump($c);

/**
*PHP Booleans
*/
$show_error = true;
var_dump($show_error);

/**
*PHP Arrays
*/
$color = array("red", "green", "blue");
var_dump($color);


$color_code = array(
   "Red" => "01",
   "Green" => "02",
   "Blue" => "03"
);
var_dump($color_code);

/**
*PHP Objects
*/
/**
*declaring class
*/
class greetings{

    public $str = "hello world";

    /**method*/
    function show_greeting(){
        return $this->$str;
    }
}

/**
*creating object of class
*/
$obj_new = new greetings;
var_dump($obj_new);
$obj_new1 = new greetings;
var_dump($obj_new1);

/**
*PHP Null
*/
$a = NULL;
var_dump($a);
 
$b = "Hello World!";
$b = NULL;
var_dump($b);

?>
