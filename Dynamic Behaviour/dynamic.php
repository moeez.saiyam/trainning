<?php

$a = 'hello';
$$a = 'world';
echo "$a ${$a} "."\n";

  $Bar = "b";
  $Foo = "Bar";
  $World = "Foo";
  $Hello = "World";
  $b = "Hello";

  echo $b; //Returns Hello
  echo $$b; //Returns World
  echo $$$b; //Returns Foo
  echo $$$$b; //Returns Bar
  echo $$$$$b; //Returns a

  echo $$$$$$b; //Returns Hello
  echo $$$$$$$b; //Returns World

?>
