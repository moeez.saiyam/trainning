<?php
/**
* Implementation of Factory Pattern
*/

/**
* Automobile interface that has method named details
*/
interface Automobile {
    public function details();
}

/**
* Class Car
*/
class Car implements Automobile {
    
    public function details () {
        echo "Building a car \n";
    }
}

/**
* Class Bike
*/
class Bike implements Automobile {

    public function details () {
        echo "Building a bike \n";
    }
}

/**
*A factory class that take @param $build and make its instance
*/

class Factory {

    
    public function buildVehicle ($build)
    {
        if (is_subclass_of($build , 'Automobile')) {
            return new $build;
        }
        else {
            return null;
        }
    }
    
}

/** Driver */
$car = new Factory();
$bike = $car->buildVehicle("Bike");
if ($bike) {
    $bike->details();
}
?>
