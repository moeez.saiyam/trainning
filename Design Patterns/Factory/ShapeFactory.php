<?php

/**
* A shape interface having darw method
*/
interface Shape {
    public function draw();
}

/**
* Circle class
*/
class Circle implements Shape {
    public function draw()
    {
        echo "Drawing Circle";
    }
}

/**
* Square class
*/
class Square implements Shape {
    public function draw ()
    {
        echo "Drawing Square";
    }
}

/**
* Triangle class
*/
class Triangle implements Shape {
  public function draw ()
  {
      echo "Drawing Triangle";
  }
}

/**
* A factory class that take @param and make its instance
*/
class Factory
{
    private $type;

    public function __construct($type = null)
    {
        if($type)
        {
          $this->type = $type;
        }
    }

    public function setType($type)
    {
     
      if (is_subclass_of($type , 'Shape')) {
          return new $type;
      }
      else {
           throw new Exception('class not found');
           return null;
       }
	
    }
}

/** Driver */
$factory = new Factory();
$shape = $factory->setType("Circle");
if($shape){
	echo $shape->draw();
}
$factory = new Factory();
$shape = $factory->setType("Rectangle");
if($shape)
{	
	echo $shape->draw();
}



?>
