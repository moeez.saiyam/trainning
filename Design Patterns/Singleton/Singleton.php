<?php

/**
* A user class whose constructor is to be called once
*/
class User {

    private static $instance = null;
    private $id = 1;
    private $name = "Moeez";
/*
    private function __construct ($id, $name)
    {
        if ($id && $name) {

            $this->id = $id;
            $this->name = $name;
        }
    }
*/

    private function __construct ()
    {
    }

    private public function __wakeup()
    {
        throw new Exception('Feature disabled.');
    }

    private public function __clone()
    {
        throw new Exception('Feature disabled.');
    }

    private public function __sleep()
    {
        throw new Exception('Feature disabled.');
    }


    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new User ();
        }
        return self :: $instance;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId()
    {
        return $this->id;
    }

}

$o = User::getInstance();
echo "NAME: " . $o -> getName() . "\n";
echo "ID: " . $o -> getID() . "\n";
$o1 = User::getInstance();
echo "NAME: " . $o -> getName() . "\n";
echo "ID: " . $o -> getID() . "\n";

?>
