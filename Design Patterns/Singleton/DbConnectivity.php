<?php

/**
* connect DB class that implements singleton pattern
*/
class ConnectDb {

    private $link;
    private static $instance = null;
    private $dbhost = "localhost";
    private $dbuser = "root";
    private $dbpass = "123";
    private $dbname = "Student";

    private function __construct(){

        $this->link = new mysqli($this->dbhost, $this->dbuser, $this->dbpass,$this->dbname); 
        
    }

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new connectDb();
        }
        return self::$instance;
    }

    public function getConnection()
    {
        return $this->link;
    }

}

    $instance = ConnectDb::getInstance();
    $conn = $instance->getConnection();
    var_dump($conn);

?>


